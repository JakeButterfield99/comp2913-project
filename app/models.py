from app import db

class User(db.Model):
	__tablename__ = "user"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	fname = db.Column(db.String(500), nullable=False)
	lname = db.Column(db.String(500), nullable=False)
	email = db.Column(db.String(500), nullable=False)
	dob = db.Column(db.DateTime)
	password = db.Column(db.String(500), nullable=False)
	payrel = db.relationship("Payment", back_populates="userrel")
	userbookrel = db.relationship("Booking", back_populates="bookrel")
	usercardrel = db.relationship("Card", back_populates="cardrel")
	staffrel = db.relationship("Staff", back_populates="userstaffrel")
	cashrel = db.relationship("Cash", back_populates="usercashrel")

class Payment(db.Model):
	__tablename__ = "payment"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	user = db.Column(db.Integer, db.ForeignKey("user.id"))
	method = db.Column(db.String(500), nullable=False)
	type = db.Column(db.String(500), nullable=False)
	amount = db.Column(db.Integer)
	date = db.Column(db.String(100))
	userrel = db.relationship("User", back_populates="payrel")

class Timetable(db.Model):
	__tablename__ = "timetable"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	facility = db.Column(db.String(100))
	date = db.Column(db.String(100))
	time = db.Column(db.String(100))
	spaces = db.Column(db.Integer)
	staffid = db.Column(db.Integer, db.ForeignKey("staff.id"))
	staffrunrel = db.relationship("Staff", back_populates="classrel")
	bookingrel = db.relationship("Booking", back_populates="timetablerel")
	cashtimerel = db.relationship("Cash", back_populates="cashtimetablerel")

class Booking(db.Model):
	__tablename__ = "booking"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	activity = db.Column(db.String(50))
	date = db.Column(db.String(100))
	userid = db.Column(db.Integer, db.ForeignKey("user.id"))
	activityid = db.Column(db.Integer, db.ForeignKey("timetable.id"))
	timetablerel = db.relationship("Timetable", back_populates="bookingrel")
	bookrel = db.relationship("User", back_populates="userbookrel")

class Staff(db.Model):
	__tablename__ = "staff"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	userid = db.Column(db.Integer, db.ForeignKey("user.id"))
	jobrole = db.Column(db.String(500), nullable=False)
	userstaffrel = db.relationship("User", back_populates="staffrel")
	classrel = db.relationship("Timetable", back_populates="staffrunrel")

class Cash(db.Model):
	__tablename__ = "cash"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	userid = db.Column(db.Integer, db.ForeignKey("user.id"))
	activity = db.Column(db.String(100))
	activityid = db.Column(db.Integer, db.ForeignKey("timetable.id"))
	usercashrel = db.relationship("User", back_populates="cashrel")
	cashtimetablerel = db.relationship("Timetable", back_populates="cashtimerel")

class Card(db.Model):
	__tablename__ = "card"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	userid = db.Column(db.Integer, db.ForeignKey("user.id"))
	cardNo = db.Column(db.String(100))
	cvc = db.Column(db.String(100))
	expireMonth = db.Column(db.String(100))
	expireYear = db.Column(db.String(100))
	cardrel = db.relationship("User", back_populates="usercardrel")