import string
import random
import datetime
import sys
import os
import webbrowser
import hashlib

from flask import render_template, flash, redirect, url_for, session, request, make_response
from flask_mail import Message
from weasyprint import HTML
from app import app
from .forms import *

from app import db, models, mail

def GetMembership():
	loggedin = False
	if 'email' in session:
		loggedin = True

	membership = {
		"name": "Expired",
		"last-payment": "Never"
	}

	if loggedin:
		uid = session['userid']
		exists = models.Payment.query.filter_by(user=uid).all()

		if exists:
			for payment in exists:
				membershipType = payment.type
				membershipDate = payment.date

				if membershipType == "membership-monthly":
					membership["name"] = "Monthly"
				elif membershipType == "membership-annual":
					membership["name"] = "Annual"
				elif membershipType == "membership-student":
					membership["name"] = "Student"
				elif membershipType == "membership-cancel":
					membership["name"] = "Cancelled"
				elif membershipType == "membership-expired":
					membership["name"] = "Expired"
				else:
					continue

				membership["last-payment"] = membershipDate			

	return membership


def CheckStaff( userid ):
	staff = models.Staff.query.filter_by(userid=userid).first()
	if staff:
		return staff.jobrole
	else:
		return "user"


def Login( email, password ):
	exists = models.User.query.filter_by(email=email).first()

	salt = "hotdog"
	db_password = password + salt
	h = hashlib.md5(db_password.encode())
	pwd = h.hexdigest()	

	if not (exists):
		session["error_message"] = "Email could not be found"
		return redirect(url_for('index'))

	else:
		if not (pwd == exists.password):
			session["error_message"] = "Password is incorrect"
			return redirect(url_for('index'))
		else:
			# Do session things here
			session['fname'] = exists.fname
			session['lname'] = exists.lname
			session['dob'] = exists.dob
			session['email'] = email
			session['userid'] = exists.id
			session['role'] = CheckStaff( exists.id )

			session['card_num'] = ""
			session['card_cvc'] = ""
			session['card_month'] = ""
			session['card_year'] = ""	

			card = models.Card.query.filter_by(userid=exists.id).all()

			if card:
				card = card[-1]
				session['card_num'] = card.cardNo
				session['card_cvc'] = card.cvc
				session['card_month'] = card.expireMonth
				session['card_year'] = card.expireYear

			return redirect(url_for('index'))


receiptData = {}

receiptData["membership-annual"] = {
	"name": "Annual Membership",
	"desc": "Membership to Sour Skittles Gym (12 Months)",
	"price": "149.99"
}

receiptData["membership-monthly"] = {
	"name": "Monthly Membership",
	"desc": "Membership to Sour Skittles Gym (1 Month)",
	"price": "14.99"
}

receiptData["membership-student"] = {
	"name": "Student Membership",
	"desc": "Membership to Sour Skittles Gym (12 Months)",
	"price": "129.99"
}

receiptData["booking"] = {
	"name": "SS Gym Booking",
	"desc": "Booking for one of the SS Gym Classes",
	"price": "5"
}


# Create a directory in a known location to save files to.
receipts_dir = os.path.join(app.instance_path, 'receipts')
os.makedirs(receipts_dir, exist_ok=True)

@app.route('/receipt/<name>/<typ>/<method>/<paymentid>')
def receipt(name, typ, method, paymentid):
	recData = receiptData[typ]

	rendered = render_template("payment.html", name=name, recData=recData, method=method, paymentNo=paymentid)
	pdf = HTML( string=rendered ).write_pdf()

	if os.path.exists(receipts_dir):
		receiptName = "receipt-" + str(paymentid) + ".pdf"
		f = open(os.path.join(receipts_dir, receiptName), "wb")
		f.write(pdf)

	userEmail = session['email']

	if "staff_cash_process" in session:
		userEmail = session["staff_cash_email"]
		session["staff_cash_email"] = ""

	msg = Message("Receipt from your order!",
				  sender="sourskittlessoftware@gmail.com",
				  recipients=[userEmail])

	msg.body = "Please find attached the receipt from your recent order"
	msg.attach( 'receipt.pdf', 'application/pdf', pdf )

	mail.send(msg)

	if "staff_cash_process" in session:
		session["staff_cash_process"] = False
		return redirect(url_for('cash_payment'))

	return redirect(url_for('dashboard'))


@app.route('/display_receipt/<paymentid>')
def display_receipt( paymentid ):
	if os.path.exists(receipts_dir):
		receiptName = "receipt-" + str(paymentid) + ".pdf"
		webbrowser.open_new( os.path.join(receipts_dir, receiptName) )

	return redirect(url_for('dashboard'))


@app.route('/test', methods=['GET', 'POST'])
def test():
	recData = { "price": 999, "name": "Test Item", "desc": "This is a test" }
	paymentNo = 0
	return render_template('payment.html', recData=recData, paymentNo=paymentNo)


@app.route('/', methods=['GET', 'POST'])
def index():
	loggedin = False
	if 'email' in session:
		loggedin = True

	# Login Form
	login_form = LoginForm()
	if login_form.validate_on_submit() and login_form.loginSubmit.data:

		postEmail = login_form.email.data
		postPass = login_form.password.data
		
		return Login(postEmail, postPass)

	errorMessage = ""
	if "error_message" in session:
		errorMessage = session["error_message"]
		session.pop("error_message", None)

	# Render View
	return render_template('index2.html',
						   title='Home',
						   loggedin=loggedin,
						   errorMessage=errorMessage,
						   login_form=login_form)	


@app.route('/memberships', methods=['GET', 'POST'])
def memberships():
	loggedin = False
	if 'email' in session:
		loggedin = True

	membership = GetMembership()	

	# Render View
	return render_template('memberships.html',
						   title='Memberships',
						   loggedin=loggedin,
						   membership=membership)


# COME BACK TO DO - change cancel purchase to edit last one instead

@app.route('/cancelmembership', methods=['GET', 'POST'])
def cancelmembership():
	userid = session['userid']
	method = "cancel"
	typ = "membership-cancel"
	price = 0
	date = datetime.date.today()

	purchase = models.Payment(user=userid, method=method, type=typ, amount=price, date=date)
	db.session.add(purchase)
	db.session.commit()

	return redirect(url_for('dashboard'))


membershipPrice = {
	"monthly": "14.99",
	"annual": "149.99",
	"student": "129.99"
}

@app.route('/purchase', methods=['GET', 'POST'])
def purchase():
	membershipType = request.form.get('member')

	userid = session['userid']

	cardNum = request.form.get('card[number]')
	cardCvc = request.form.get('card[cvc]')
	cardExpiryMonth = request.form.get('card[expire-month]')
	cardExpiryYear = request.form.get('card[expire-year]')

	memberThing = request.form.get('member')
	typ = "membership-" + memberThing

	price = membershipPrice[memberThing]

	date = datetime.date.today()

	method = ""

	if cardNum == "":
		print("Payment Method: PayPal")
		method = "paypal"
	else:
		print("Payment Method: Card")
		method = "card"

	purchase = models.Payment(user=userid, method=method, type=typ, amount=price, date=date)
	db.session.add(purchase)
	db.session.flush()

	card = models.Card(userid=userid, cardNo=cardNum, cvc=cardCvc, expireMonth=cardExpiryMonth, expireYear=cardExpiryYear)
	db.session.add(card)
	db.session.flush()

	session['card_num'] = cardNum
	session['card_cvc'] = cardCvc
	session['card_month'] = cardExpiryMonth
	session['card_year'] = cardExpiryYear

	db.session.commit()

	cxName = session['fname'] + " " + session["lname"]
	paymentid = purchase.id

	session["member_confirm"] = True
	return redirect(url_for('receipt', name=cxName, typ=typ, method=method, paymentid=paymentid))



def MakeBooking(uid, date, time, bookingType):
	
	booking = models.Payment(user=userid, method=method, type=typ, amount=price, date=date)
	db.session.add(booking)
	db.session.commit()


def GetClasses( facility ):
	dateToday = datetime.date.today()
	dateTomorrow = dateToday + datetime.timedelta(days=1)
	dateNext = dateTomorrow + datetime.timedelta(days=1)

	dates = [dateToday, dateTomorrow, dateNext]

	fitness_today = models.Timetable.query.filter_by(facility="fitness", date=dateToday).all()
	fitness_tomorrow = models.Timetable.query.filter_by(facility="fitness", date=dateTomorrow).all()
	fitness_next = models.Timetable.query.filter_by(facility="fitness", date=dateNext).all()
	fitness_array = fitness_today + fitness_tomorrow + fitness_next

	swimming_today = models.Timetable.query.filter_by(facility="swimming", date=dateToday).all()
	swimming_tomorrow = models.Timetable.query.filter_by(facility="swimming", date=dateTomorrow).all()
	swimming_next = models.Timetable.query.filter_by(facility="swimming", date=dateNext).all()
	swimming_array = swimming_today + swimming_tomorrow + swimming_next

	squash_today = models.Timetable.query.filter_by(facility="squash", date=dateToday).all()
	squash_tomorrow = models.Timetable.query.filter_by(facility="squash", date=dateTomorrow).all()
	squash_next = models.Timetable.query.filter_by(facility="squash", date=dateNext).all()
	squash_array = squash_today + squash_tomorrow + squash_next

	sports_today = models.Timetable.query.filter_by(facility="sports", date=dateToday).all()
	sports_tomorrow = models.Timetable.query.filter_by(facility="sports", date=dateTomorrow).all()
	sports_next = models.Timetable.query.filter_by(facility="sports", date=dateNext).all()
	sports_array = sports_today + sports_tomorrow + sports_next

	# Sort Stuff

	booking_dict = {}
	booking_num = {}

	booking_num["fitness"] = 0
	booking_num["swimming"] = 0
	booking_num["squash"] = 0
	booking_num["sports"] = 0

	if len(fitness_array) > 0:
		booking_dict["fitness"] = []

	if len(swimming_array) > 0:
		booking_dict["swimming"] = []

	if len(squash_array) > 0:
		booking_dict["squash"] = []

	if len(sports_array) > 0:
		booking_dict["sports"] = []		

	for fitness in fitness_array:
		if not fitness.date in booking_dict["fitness"]:
			booking_dict["fitness"].append(fitness.date)
			booking_num["fitness"] = booking_num["fitness"] + 1

	for swimming in swimming_array:
		if not swimming.date in booking_dict["swimming"]:
			booking_dict["swimming"].append(swimming.date)
			booking_num["swimming"] = booking_num["swimming"] + 1

	for squash in squash_array:
		if not squash.date in booking_dict["squash"]:
			booking_dict["squash"].append(squash.date)
			booking_num["squash"] = booking_num["squash"] + 1

	for sports in sports_array:
		if not sports.date in booking_dict["sports"]:
			booking_dict["sports"].append(sports.date)
			booking_num["sports"] = booking_num["sports"] + 1

	if facility == "all":
		return booking_dict, booking_num
	else:
		return booking_dict[facility], booking_num[facility]


@app.route('/classes_and_facilities', methods=['GET', 'POST'])
def classes_and_facilities():
	loggedin = False
	if 'email' in session:
		loggedin = True

	booking_dict, booking_num = GetClasses("all")

	errorMessage = ""
	if "classes_error" in session:
		errorMessage = session['classes_error']
		session.pop('classes_error', None)		

	# Render View
	return render_template('classes_and_facilities.html',
						   title='Classes & Facilities',
						   booking_dict=booking_dict,
						   booking_num=booking_num,
						   errorMessage=errorMessage,
						   loggedin=loggedin)		


@app.route('/confirm_booking', methods=['GET', 'POST'])
def confirm_booking():
	loggedin = False
	if 'email' in session:
		loggedin = True

	chosenWeekly = session["booking_data"]["chosenWeekly"]

	price = 5
	if chosenWeekly == "yes":
		price = 200

	return render_template('confirm_booking.html',
						   title='Confirm Booking',
						   chosenWeekly=chosenWeekly,
						   price=price,
						   loggedin=loggedin)


@app.route('/process_booking', methods=['GET', 'POST'])
def process_booking():
	chosenDate = request.form.get('dateChosen')
	chosenTime = request.form.get('timeChosen')
	chosenActivity = request.form.get('choose')

	chosenWeekly = "no"
	
	if request.form.get('weekly'):
		chosenWeekly = "yes"

	chosenFacility = ""

	if request.form.get('fitness_submit'):
		chosenFacility = "fitness"
	elif request.form.get('swimming_submit'):
		chosenFacility = "swimming"
	elif request.form.get('squash_submit'):
		chosenFacility = "squash"
	elif request.form.get('sports_submit'):
		chosenFacility = "sports"

	booking_data = {}
	booking_data["chosenDate"] = chosenDate
	booking_data["chosenTime"] = chosenTime
	booking_data["chosenActivity"] = chosenActivity
	booking_data["chosenFacility"] = chosenFacility
	booking_data["chosenWeekly"] = chosenWeekly

	exists = models.Timetable.query.filter_by(date=chosenDate, time=chosenTime, facility=chosenFacility).first()

	if ( exists ):
		timetableID = exists.id
		userCheck = models.Booking.query.filter_by(activityid=timetableID).first()

		if ( userCheck ):
			session['classes_error'] = "Sorry, this time slot is not available"
			return redirect(url_for('classes_and_facilities'))
		else:
			booking_data["timetable"] = timetableID

	else:
		session['classes_error'] = "Something went wrong, please try again"
		return redirect(url_for('classes_and_facilities'))	

	session['booking_data'] = booking_data

	return redirect(url_for('confirm_booking'))	


@app.route('/make_booking', methods=['GET', 'POST'])
def make_booking():
	# Card Details
	cardNum = request.form.get('card[number]')
	cardCvc = request.form.get('card[cvc]')
	cardExpiryMonth = request.form.get('card[expire-month]')
	cardExpiryYear = request.form.get('card[expire-year]')

	method = ""

	if request.form.get('cash_submit'):
		method = "cash"
	elif request.form.get('card_submit'):
		method = "card"
	elif request.form.get('paypal_submit'):
		method = "paypal"

	typ = "booking"

	# Booking Data
	booking_data = session['booking_data']
	chosenDate = booking_data["chosenDate"]
	chosenTime = booking_data["chosenTime"]
	chosenActivity = booking_data["chosenActivity"]
	chosenFacility = booking_data["chosenFacility"]
	chosenWeekly = booking_data["chosenWeekly"]

	price = 5
	if chosenWeekly == "yes":
		price = 200	

	timetableID = booking_data["timetable"]

	# User Data
	userid = session['userid']

	if method == "cash":
		
		cash = models.Cash(userid=userid, activity=chosenActivity, activityid=timetableID)
		db.session.add(cash)
		db.session.commit()

		session["cash_confirm"] = True
		return redirect(url_for('dashboard'))

	else:

		date = datetime.date.today()

		print("New Booking | " + chosenDate + " | " + chosenTime + " | " + chosenActivity + " | " + chosenFacility + " from user " + str(userid) + " using card no: " + str(cardNum) )

		purchase = models.Payment(user=userid, method=method, type=typ, amount=price, date=date)
		db.session.add(purchase)
		db.session.flush()

		if method == "card":

			card = models.Card(userid=userid, cardNo=cardNum, cvc=cardCvc, expireMonth=cardExpiryMonth, expireYear=cardExpiryYear)
			db.session.add(card)
			db.session.flush()

			session['card_num'] = cardNum
			session['card_cvc'] = cardCvc
			session['card_month'] = cardExpiryMonth
			session['card_year'] = cardExpiryYear

		newActivity = models.Booking( activity=chosenActivity, date=date, userid=userid, activityid=timetableID )
		db.session.add(newActivity)
				
		db.session.commit()

		cxName = session['fname'] + " " + session["lname"]
		paymentid = purchase.id

		session["classes_confirm"] = True
		return redirect(url_for('receipt', name=cxName, typ=typ, method=method, paymentid=paymentid))


@app.route('/signup', methods=['GET', 'POST'])
def signup():

	loggedin = False
	if 'email' in session:
		loggedin = True

	# Login Form
	login_form = LoginForm()
	if login_form.validate_on_submit() and login_form.loginSubmit.data:

		postEmail = login_form.email.data
		postPass = login_form.password.data
		
		return Login(postEmail, postPass)

	if 'email' in session:
		return redirect(url_for('index'))

	# Register Form
	register_form = SignupForm()
	if register_form.validate_on_submit() and register_form.registerSubmit.data:

		exists = models.User.query.filter_by(email=register_form.email.data).first()
		
		if not (register_form.password.data == register_form.confirmPassword.data):
			flash("Your passwords did not match!")

		elif ( exists ):
			flash("That email is already registered!")

		else:

			user_entered_password = register_form.password.data
			salt = "hotdog"
			db_password = user_entered_password + salt
			h = hashlib.md5(db_password.encode())
			pwd = h.hexdigest()

			register = models.User(fname=register_form.fname.data, lname=register_form.lname.data, email=register_form.email.data, password=pwd, dob=register_form.dob.data,)
			db.session.add(register)
			db.session.commit()

			return redirect(url_for('index'))	

	# Render View
	return render_template('signup.html',
						   title='Sign Up',
						   register_form=register_form,
						   login_form=login_form,
						   loggedin=loggedin)


facilityChoices = {
	'fitness': 'Fitness Room',
	'swimming': 'Swimming Pool',
	'squash': 'Squash Court',
	'sports': 'Sports Hall'
}

activityChoices = {
	'general': 'General Session (1 Hour)',
	'lane': 'Lane Swimming',
	'lesson': 'Lesson',
	'team': 'Team Event'
}                    

@app.route('/dashboard', methods=['GET', 'POST'])
def dashboard():
	loggedin = False
	if 'email' in session:
		loggedin = True

	membership = GetMembership()
	payments = {}
	uid = session['userid']
	payment_query = models.Payment.query.filter_by(user=uid).all()

	for payment in payment_query:
		payments[payment.id] = {}

		pType = ""
		if "membership" in payment.type:
			pType = "Membership"
		else:
			pType = "Booking"

		payments[payment.id]["type"] = pType
		payments[payment.id]["date"] = payment.date

	bookings = {}
	booking_query = models.Booking.query.filter_by(userid=uid).all()

	for booking in booking_query:
		bookings[booking.id] = {}

		facility_query = models.Timetable.query.filter_by(id=booking.activityid).first()

		bType = activityChoices[booking.activity]
		bDate = facility_query.date
		bTime = facility_query.time
		bFac = facilityChoices[facility_query.facility]

		bookings[booking.id]["type"] = bType
		bookings[booking.id]["date"] = bDate
		bookings[booking.id]["time"] = bTime
		bookings[booking.id]["facility"] = bFac		

	booking_confirm = False
	if "classes_confirm" in session:
		booking_confirm = True
		session.pop('classes_confirm', None)

	member_confirm = False
	if "member_confirm" in session:
		member_confirm = True
		session.pop('member_confirm', None)	

	cash_confirm = False
	if "cash_confirm" in session:
		cash_confirm = True
		session.pop('cash_confirm', None)

	# Render View
	return render_template('dashboard.html',
						   title='Dashboard',
						   membership=membership,
						   payments=payments,
						   bookings=bookings,
						   member_confirm=member_confirm,
						   booking_confirm=booking_confirm,
						   cash_confirm=cash_confirm,
						   loggedin=loggedin)


#
# Staff Stuff
#

@app.route('/staff', methods=['GET', 'POST'])
def staff():
	staffRole = session['role']
	if staffRole == "user":
		return redirect(url_for('index'))

	loggedin = False
	if 'email' in session:
		loggedin = True		

	return render_template('staff.html',
							title='Staff Center',
							loggedin=loggedin)


@app.route('/view_schedule', methods=['GET', 'POST'])
def view_schedule():
	staffRole = session['role']
	if staffRole == "user":
		return redirect(url_for('index'))

	loggedin = False
	if 'email' in session:
		loggedin = True	

	uid = session['userid']

	bookings = {}

	staff = models.Staff.query.filter_by(userid=uid).first()
	staffid = staff.id

	timetable_query = models.Timetable.query.filter_by(staffid=staffid).all()
	for timetable in timetable_query:
		bookings[timetable.id] = {}
		bookings[timetable.id]["date"] = timetable.date
		bookings[timetable.id]["time"] = timetable.time
		bookings[timetable.id]["facility"] = timetable.facility

		booking_query = models.Booking.query.filter_by( activityid=timetable.id ).first()

		bookings[timetable.id]["activity"] = booking_query.activity

	return render_template('schedule.html',
							title='Schedule',
							bookings=bookings,
							loggedin=loggedin)

@app.route('/manage_staff', methods=['GET', 'POST'])
def manage_staff():
	staffRole = session['role']
	if staffRole == "user":
		return redirect(url_for('index'))
	elif staffRole == "staff":
		return redirect(url_for('staff'))

	loggedin = False
	if 'email' in session:
		loggedin = True		

	staffMembers = {}
	staff_query = models.Staff.query.all()
	for staff in staff_query:
		staffMembers[staff.id] = {}

		user = models.User.query.filter_by(id=staff.userid).first()

		staffMembers[staff.id]["name"] = (user.fname + " " + user.lname)
		staffMembers[staff.id]["role"] = staff.jobrole

	bookings = {}
	booking_query = models.Booking.query.all()
	for booking in booking_query:
		bookings[booking.id] = {}

		timetable = models.Timetable.query.filter_by(id=booking.activityid).first()

		bookings[booking.id]["activity"] = booking.activity
		bookings[booking.id]["facility"] = timetable.facility
		bookings[booking.id]["date"] = timetable.date
		bookings[booking.id]["time"] = timetable.time
		bookings[booking.id]["timetable"] = timetable.id


	success_message = ""
	error_message = ""

	if "success_message" in session:
		success_message = session['success_message']
		session.pop('success_message', None)

	if "error_message" in session:
		error_message = session['error_message']
		session.pop('error_message', None)		

	return render_template('manage_staff.html',
							title='Manage Staff',
							staffMembers=staffMembers,
							bookings=bookings,
							success_message=success_message,
							error_message=error_message,
							loggedin=loggedin)


@app.route('/promote_staff/<id>', methods=['GET', 'POST'])
def promote_staff(id):
	return redirect(url_for('edit_role', newrole="manager", id=id))


@app.route('/demote_staff/<id>', methods=['GET', 'POST'])
def demote_staff(id):
	staff = models.Staff.query.filter_by(id=id).first()
	newrole = ""

	if staff.jobrole == "manager":
		newrole = "staff"
	else:
		newrole = "user"

	return redirect(url_for('edit_role', newrole=newrole, id=id))


@app.route('/edit_role/<newrole>/<id>', methods=['GET', 'POST'])
def edit_role(newrole, id):
	staff = models.Staff.query.filter_by(id=id).first()
	if newrole == "user":
		db.session.delete(staff)
	else:
		staff.jobrole = newrole
	
	db.session.commit()

	session['success_message'] = "Staff member's role has been changed, new role: " + newrole
	return redirect(url_for('manage_staff'))


@app.route('/new_staff', methods=['GET', 'POST'])
def new_staff():
	email = request.form.get('email')

	user = models.User.query.filter_by(email=email).first()

	staff = models.Staff(userid=user.id, jobrole="staff")
	db.session.add(staff)
	db.session.commit()

	session['success_message'] = "Staff member has been given access!"
	return redirect(url_for('manage_staff'))


@app.route('/assign_staff/<booking>', methods=['GET', 'POST'])
def assign_staff(booking):
	staffid = request.form.get('staff_member')

	timetable = models.Timetable.query.filter_by(id=booking).first()
	timetable.staffid = staffid
	db.session.commit()

	session['success_message'] = "Staff member has been assigned to that booking!"
	return redirect(url_for('manage_staff'))


@app.route('/cash_payment', methods=['GET', 'POST'])
def cash_payment():
	loggedin = False
	if 'email' in session:
		loggedin = True

	staffRole = session['role']
	if staffRole == "user":
		return redirect(url_for('dashboard'))

	cashUsers = {}

	cashBuyers = models.Cash.query.all()

	for buyer in cashBuyers:
		user = models.User.query.filter_by(id=buyer.userid).first()
		name = user.fname + " " + user.lname
		cashUsers[buyer.id] = name

	return render_template('cash_payment.html',
							title='Cash Payment',
							cashUsers=cashUsers,
							loggedin=loggedin)


@app.route('/approve_cash/<id>', methods=['GET', 'POST'])
def approve_cash(id):
	date = datetime.date.today()

	cash = models.Cash.query.filter_by(id=id).first()

	activityid = cash.activityid

	uid = cash.userid

	timetable = models.Timetable.query.filter_by(id=activityid).first()

	chosenDate = timetable.date
	chosenTime = timetable.time
	chosenActivity = cash.activity
	chosenFacility = timetable.facility

	print("New Booking | " + chosenDate + " | " + chosenTime + " | " + chosenActivity + " | " + chosenFacility + " from user " + str(uid) + " using cash" )

	typ = "booking"
	price = 5
	method = "cash"

	purchase = models.Payment(user=uid, method=method, type=typ, amount=price, date=date)
	db.session.add(purchase)
	db.session.flush()

	newActivity = models.Booking( activity=chosenActivity, date=date, userid=uid, activityid=activityid )
	db.session.add(newActivity)
			
	db.session.commit()

	user = models.User.query.filter_by(id=cash.userid).first()
	cxName = user.fname + " " + user.lname
	paymentid = purchase.id

	db.session.delete(cash)
	db.session.commit()	

	session["staff_cash_process"] = True
	session["staff_cash_email"] = user.email
	return redirect(url_for('receipt', name=cxName, typ=typ, method=method, paymentid=paymentid))	


@app.route('/make_staff', methods=['GET', 'POST'])
def make_staff():
	uid = session['userid']

	staff = models.Staff(userid=uid, jobrole="staff")
	db.session.add(staff)
	db.session.commit()

	session['role'] = "staff"

	return redirect(url_for('dashboard'))


@app.route('/make_user', methods=['GET', 'POST'])
def make_user():
	uid = session['userid']

	staff = models.Staff.query.filter_by(userid=uid).first()
	db.session.delete(staff)
	db.session.commit()

	session['role'] = "user"

	return redirect(url_for('dashboard'))	


@app.route('/make_staff_manager', methods=['GET', 'POST'])
def make_staff_manager():
	uid = session['userid']

	staff = models.Staff.query.filter_by(userid=uid).first()
	staff.jobrole = "staff"
	db.session.commit()

	session['role'] = "staff"

	return redirect(url_for('staff'))


@app.route('/make_manager', methods=['GET', 'POST'])
def make_manager():
	uid = session['userid']

	staff = models.Staff.query.filter_by(userid=uid).first()
	staff.jobrole = "manager"
	db.session.commit()

	session['role'] = "manager"

	return redirect(url_for('staff'))	


activityList = {
	'fitness': ['general'],
	'sports': ['general'],
	'swimming': ['general', 'lane', 'lesson', 'team'],
	'squash': ['general', 'team']
}

activitySpaces = {
	'fitness': { 'general': 50 },
	'sports': { 'general': 50 },
	'swimming': { 'general': 50, 'lane': 20, 'lesson': 6, 'team': 12 },
	'squash': { 'general': 50, 'team': 4 }
}

@app.route('/staff_manage_activities', methods=['GET', 'POST'])
def staff_manage_activities():
	loggedin = False
	if 'email' in session:
		loggedin = True

	staffRole = session['role']
	if staffRole == "user":
		return redirect(url_for('index'))

	if staffRole == "staff":
		return redirect(url_for('staff'))

	dateToday = datetime.date.today()
	dateTomorrow = dateToday + datetime.timedelta(days=1)
	dateNext = dateTomorrow + datetime.timedelta(days=1)

	dates = [dateToday, dateTomorrow, dateNext]

	return render_template('staff_manage_activities.html',
							title='Manage Activities',
							loggedin=loggedin,
							dates=dates)	


@app.route('/manage_diary', methods=['GET', 'POST'])
def manage_diary():
	dateToday = datetime.date.today()
	dateTomorrow = dateToday + datetime.timedelta(days=1)
	dateNext = dateTomorrow + datetime.timedelta(days=1)

	dates = [dateToday, dateTomorrow, dateNext]	

	chosenFacility = request.form.get('chosenFacility')
	chosenDate = request.form.get('chosenDate')			

	if chosenFacility == "none":
		return redirect(url_for('staff_manage_activities'))

	if chosenDate == "all":
		days = [dateToday, dateTomorrow, dateNext]
	else:
		days = [chosenDate]

	timeChoices = ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00']		

	curActivity = 0
	# Loop through all days
	for eachDay in days:
		# Loop through all times
		for eachTime in timeChoices:
			newActivity = models.Timetable( date=eachDay, time=eachTime, spaces=50, facility=chosenFacility )
			db.session.add(newActivity)
			db.session.flush()
			
	db.session.commit()

	return redirect(url_for('staff_manage_activities'))


@app.route('/usage', methods=['GET', 'POST'])
def usage():
	staffRole = session['role']
	if (staffRole == "user"):
		return redirect(url_for('index'))
	elif (staffRole == "staff"):
		return redirect(url_for('staff'))		

	loggedin = False
	if 'email' in session:
		loggedin = True		

	# Get Stats

	stats = {}
	stats["bookings"] = {}
	stats["bookings"]["total"] = 0
	stats["bookings"]["general"] = 0
	stats["bookings"]["team"] = 0
	stats["bookings"]["lane"] = 0
	stats["bookings"]["lesson"] = 0

	stats["payment"] = {}
	stats["payment"]["total"] = 0
	stats["payment"]["membership"] = 0
	stats["payment"]["bookings"] = 0

	day_one = datetime.date.today()
	day_two = day_one - datetime.timedelta(days=1)
	day_three = day_two - datetime.timedelta(days=1)
	day_four = day_three - datetime.timedelta(days=1)
	day_five = day_four - datetime.timedelta(days=1)
	day_six = day_five - datetime.timedelta(days=1)
	day_seven = day_six - datetime.timedelta(days=1)

	dates = [day_one, day_two, day_three, day_four, day_five, day_six, day_seven]

	for eachDay in dates:
		booking_query = models.Booking.query.filter_by(date=eachDay).all()
		payment_query = models.Payment.query.filter_by(date=eachDay).all()

		if booking_query:
			stats["bookings"]["total"] = stats["bookings"]["total"] + len(booking_query)
			for booking in booking_query:
				if "general" in booking.activity:
					stats["bookings"]["general"] = stats["bookings"]["general"] + 1
				elif "lane" in booking.activity:
					stats["bookings"]["lane"] = stats["bookings"]["lane"] + 1
				elif "team" in booking.activity:
					stats["bookings"]["team"] = stats["bookings"]["team"] + 1
				elif "lesson" in booking.activity:
					stats["bookings"]["lesson"] = stats["bookings"]["lesson"] + 1													

		if payment_query:
			for payment in payment_query:
				stats["payment"]["total"] = stats["payment"]["total"] + payment.amount
				if ("membership" in payment.type):
					stats["payment"]["membership"] = stats["payment"]["membership"] + payment.amount
				elif ("booking" in payment.type):
					stats["payment"]["bookings"] = stats["payment"]["bookings"] + payment.amount					


	stats["payment"]["total"] = float("{:.2f}".format(stats["payment"]["total"]))
	stats["payment"]["membership"] = float("{:.2f}".format(stats["payment"]["membership"]))
	stats["payment"]["bookings"] = float("{:.2f}".format(stats["payment"]["bookings"]))

	return render_template('usage.html',
							title='Usage & Income',
							loggedin=loggedin,
							stats=stats)



@app.route('/logout', methods=['GET', 'POST'])
def logout():
	session.pop('email', None)
	session.pop('userid', None)
	session.pop('fname', None)
	session.pop('lname', None)
	session.pop('dob', None)
	return redirect(url_for('index'))