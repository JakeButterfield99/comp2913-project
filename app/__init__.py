from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mail import Mail

app = Flask(__name__)
app.config.from_object('config')

app.config.update(dict(
    DEBUG = True,
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 587,
    MAIL_USE_TLS = True,
    MAIL_USE_SSL = False,
    MAIL_USERNAME = 'sourskittlessoftware@gmail.com',
    MAIL_PASSWORD = 'Sour.Skittles1',
))

db = SQLAlchemy(app)
migrate = Migrate(app, db)
mail = Mail(app)

from app import views
from app import models